package net.xeill.elpuig;

import java.io.*;
import java.util.*;


public class Marca {

    int id;
    String name;

    Marca(int id, String name, String description, int foundation) {
        this.id = id;
        this.name = name;
    }

    public void print() {
        System.out.println("ID: "+id);
        System.out.println("Nombre: "+name);
    }

}