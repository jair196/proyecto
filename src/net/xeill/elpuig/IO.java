package net.xeill.elpuig;

import java.io.*;
import java.util.*;

public class IO {

    Scanner scanner = new Scanner(System.in);

    /**
     * Clears the screen.
     */
    void cls() {
        System.out.print("\033\143");
    }

    public void welcome() {
        System.out.println("------- Catalogo -------");
        System.out.println("1 - Zapatillas");
        System.out.println("2 - Gorras");
        System.out.println("3 - Salir");
    }

    public int getWelcome() {
        String soption;
        int option;
        try {
            soption = scanner.nextLine();
            option = Integer.parseInt(soption);
        } catch (Exception e) {
            cls();
            welcome();
            return getWelcome();
        }
        return option;
    }


    void Zapatillas() {
        List<Zapatillas> zapatillas = new ArrayList<>();
        String opcio;

        System.out.println("------- Zapatillas -------");
        System.out.println("1 - Añadir zapatillas al stock");
        System.out.println("2 - Eliminar zapatillas del stock");
        System.out.println("3 - Mostrar stock");
        System.out.println("4 - Volver");
        opcio = scanner.nextLine();

        switch (opcio){
            case "1": {

                // Creamos el objeto Zapatillas
                Zapatillas s = new Zapatillas();
                // Rellenamos el objeto zapatillas con los valores introducidos por teclado
                System.out.println("Introduce la id de la zapatilla:");
                // Pedimos un int para el ID del la zapatilla
                s.idZapatilla = scanner.nextInt();
                scanner.nextLine();

                System.out.println("Introduce la talla de la zapatilla:");
                s.talla = scanner.nextInt();

                // Consumir el retorno de carro que deja el entero
                scanner.nextLine();
                // Pedimos el modelo de la zapatilla
                System.out.println("Introduce el modelo de la zapatilla:");
                // Pedimo un int para el ID del soci
                s.modelo = scanner.nextLine();

                System.out.println("Introduce la marcade la zapatilla:");
                s.marca = scanner.nextInt();

                zapatillas.add(s);

                break;
            }
            case "2": {

                // Solicitar la id de la zapatilla
                System.out.println("Introduce la id de las zapatillas:");
                int rSoci = scanner.nextInt();
                // Eliminar del stock

                Iterator<Zapatillas> it = zapatillas.iterator();

                while(it.hasNext()){
                    Zapatillas s=it.next();
                    if (s.idZapatilla == rSoci) {
                        it.remove();
                    }
                }

                break;
            }
            case "3": {
                // https://es.stackoverflow.com/questions/51937/recorrer-un-arraylist-de-items
                // Mostrar todas las zapatillas

                Iterator<Zapatillas> it = zapatillas.iterator();
                // mientras al iterador queda proximo juego
                while(it.hasNext()){

                    Zapatillas s=it.next();


                    System.out.println(s.idZapatilla);
                    System.out.println(s.modelo);
                    System.out.println("----------");
                }

                break;
            }
            case "4": {
                break;
            }
        }
    }

    public int getZapatillasOpcion() {
        String soption;
        int option;
        try {
            soption = scanner.nextLine();
            option = Integer.parseInt(soption);
        } catch (Exception e) {
            cls();
            Zapatillas();
            return getZapatillasOpcion();

        }
        return option;
    }


    void Gorras() {
        List<Gorras> gorras = new ArrayList<>();
        String opcio;

        System.out.println("------- Gorras -------");
        System.out.println("1 - Añadir gorras al stock");
        System.out.println("2 - Eliminar gorras del stock");
        System.out.println("3 - Mostrar stock");
        System.out.println("4 - Volver");

        opcio = scanner.nextLine();

        switch (opcio){
            case "1": {

                // Creamos el objeto Gorras
                Gorras s = new Gorras();
                // Rellenamos el objeto Gorra con los valores introducidos por teclado
                System.out.println("Introduce la id de la gorra:");
                // Pedimos un int para el ID del la gorra
                s.idGorra = scanner.nextInt();
                // Consumir el retorno de carro que deja el entero
                scanner.nextLine();

                System.out.println("Introduce la talla de la gorra:");
                s.talla = scanner.nextInt();
                scanner.nextLine();

                System.out.println("Introduce el color de la gorra:");
                s.color= scanner.nextLine();

                // Pedimos el modelo de la gorra
                System.out.println("Introduce el modelo de la gorra:");
                // Pedimo un int para el ID del soci
                s.marca= scanner.nextLine();

                gorras.add(s);

                break;
            }
            case "2": {

                // Solicitar la id de la zapatilla
                System.out.println("Introduce la id de las zapatillas:");
                int rSoci = scanner.nextInt();
                // Eliminar del stock

                Iterator<Gorras> it = gorras.iterator();

                while(it.hasNext()){
                    Gorras s=it.next();
                    if (s.idGorra == rSoci) {
                        it.remove();
                    }
                }

                break;
            }
            case "3": {
                // https://es.stackoverflow.com/questions/51937/recorrer-un-arraylist-de-items
                // Mostrar todas las zapatillas

                Iterator<Gorras> it = gorras.iterator();
                // mientras al iterador queda proximo juego
                while(it.hasNext()){

                    Gorras s=it.next();


                    System.out.println(s.idGorra);
                    System.out.println(s.marca);
                    System.out.println("----------");
                }

                break;
            }
            case "4": {
                break;
            }
        }


    }

    public int getGorrasOpcion() {
        String soption;
        int option;
        try {
            soption = scanner.nextLine();
            option = Integer.parseInt(soption);
        } catch (Exception e) {
            cls();
            Gorras();
            return getGorrasOpcion();



        }
        return option;
    }

    void Salir() {
        System.exit(0);
    }


}
