package net.xeill.elpuig;


public class Zapatillas {

    int idZapatilla;
    int talla;
    String modelo;
    String marca;

    Zapatillas(int idZapatilla, int talla, String modelo, String marca) {
        this.idZapatilla = idZapatilla;
        this.talla = talla;
        this.modelo = modelo;
        this.marca = marca;
    }



    public void print() {
        System.out.println("ID: "+idZapatilla);
        System.out.println("Talla: "+talla);
        System.out.println("Modelo: "+modelo);
        System.out.println("Marca: "+ marca);
    }

}
