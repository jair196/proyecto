package net.xeill.elpuig;

public class Gorras {

    int idGorra;
    int talla;
    String color;
    String marca;

    Gorras(int idGorra, int talla, String color, String marca) {
        this.idGorra = idGorra;
        this.talla = talla;
        this.color = color;
        this.marca = marca;
    }


    public void print() {
        System.out.println("ID: "+idGorra);
        System.out.println("Talla: "+talla);
        System.out.println("Color: "+color);
        System.out.println("Marca: "+ marca);
    }

}
